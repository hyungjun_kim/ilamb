ILAMB: The ILAMB Benchmarking System
====================================

The International Land Model Benchmarking (ILAMB) project is a
model-data intercomparison and integration project designed to improve
the performance of land models and, in parallel, improve the design of
new measurement campaigns to reduce uncertainties associated with key
land surface processes. Building upon past model evaluation studies,
the goals of ILAMB are to:

* develop internationally accepted benchmarks for land model
  performance, promote the use of these benchmarks by the
  international community for model intercomparison,
* strengthen linkages between experimental, remote sensing, and
  climate modeling communities in the design of new model tests and
  new measurement programs, and
* support the design and development of a new, open source,
  benchmarking software system for use by the international community.

It is the last of these goals to which this repository is
concerned. We have developed a python-based generic benchmarking
system. Please consult the `documentation
<http://climate.ornl.gov/~ncf/ILAMB/docs/index.html>`_ for
instructions on how to install the package and get started. To get an
idea of the scope and magnitude of the project, see a sampling of the
output `here
<http://www.climatemodeling.org/~nate/ILAMB/index.html>`_.
